# dentist
[![pipeline status](https://gitlab.com/io1-dentist/dentist/badges/master/pipeline.svg)](https://gitlab.com/io1-dentist/dentist/-/commits/master)

# Live

[Live application hosted on netlify](https://dentist-io1.netlify.app/)

# Run

To run in docker container use docker compose command in root folder:
```
docker-compose up --build
```

Java project works on port 8080

Angular project using nginx on port 90

use [http://localhost:90](http://localhost:90) to start work with application

## Authors:
- mes
- ave
- nijjan