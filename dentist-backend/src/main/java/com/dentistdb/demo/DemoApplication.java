package com.dentistdb.demo;

import com.dentistdb.demo.domain.data.Patient;
import com.dentistdb.demo.domain.data.MyService;
import com.dentistdb.demo.domain.data.Treatment;
import com.dentistdb.demo.domain.data.Visit;
import com.dentistdb.demo.domain.repos.PatientRepository;
import com.dentistdb.demo.domain.repos.ServiceRepository;
import com.dentistdb.demo.domain.repos.TreatmentRepository;
import com.dentistdb.demo.domain.repos.VisitRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(PatientRepository patient, ServiceRepository service, TreatmentRepository treatment, VisitRepository visit) {
        return args -> {
            Patient patient1 = new Patient("Jan", "Kowalski", "24/02/1990", "Mężczyzna",
                    "42-400", "Warszawa", "Broniewskiego", "25/22", 123123123, "12345678900");
//            Visit visit1 = new Visit(patient1, "24-02-2020", "20:15", "info");
//            MyService service1 = new MyService("type", "name", 99.99);
            patient.save(patient1);

            patient.save(new Patient("Jan2", "Kowalski", "24/02/1990", "Mężczyzna",
                    "42-400", "Warszawa", "Broniewskiego", "25/22", 123123123, "12345678901"));

            patient.save(new Patient("Jan3", "Nowak", "24/02/1990", "Mężczyzna",
                    "42-400", "Warszawa", "Broniewskiego", "25/22", 123123123, "12345678902"));

//            service.save(service1);

//            visit.save(visit1);

//            treatment.save(new Treatment(visit1, service1, "opis"));
        };
    }
}
