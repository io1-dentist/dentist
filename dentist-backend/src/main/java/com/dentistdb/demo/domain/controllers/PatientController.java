package com.dentistdb.demo.domain.controllers;

import com.dentistdb.demo.domain.data.Patient;
import com.dentistdb.demo.domain.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/patients")
    public List<Patient> getAllPatients() {
        return patientService.getAllPatients();
    }

    @GetMapping("/patientById/{id}")
    public Patient getPatientById(@PathVariable Long id) {
        return patientService.getPatientById(id);
    }

    @GetMapping("/patientByPesel/{pesel}")
    public Patient getPatientByPesel(@PathVariable String pesel) {
        return patientService.getPatientByPesel(pesel);
    }

    @GetMapping("/patientBySecondName/{secondName}")
    public  List<Patient> getPatientBySecondName(@PathVariable String secondName) {
        return patientService.getPatientBySecondName(secondName);
    }

    @PostMapping("/addPatient")
    public void addPatient(@RequestBody Patient user) {
        patientService.addPatient(user);
    }

    @PutMapping("/editPatient")
    public void editPatient(@RequestBody Patient user) {
        patientService.editPatient(user);
    }

    @DeleteMapping("/deletePatient/{id}")
    public void deletePatient(@PathVariable Long id) {
        patientService.deletePatient(id);
    }
}
