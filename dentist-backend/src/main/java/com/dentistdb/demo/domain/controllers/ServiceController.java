package com.dentistdb.demo.domain.controllers;

import com.dentistdb.demo.domain.data.MyService;
import com.dentistdb.demo.domain.services.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @GetMapping("/services")
    public List<MyService> getAllServices() {
        return serviceService.getAllServices();
    }

    @GetMapping("/serviceById/{id}")
    public MyService getServiceById(@PathVariable Long id) {
        return serviceService.getServiceById(id);
    }

    @GetMapping("/serviceByName/{serviceName}")
    public MyService getServiceByLogin(@PathVariable String serviceName) {
        return serviceService.getServiceByName(serviceName);
    }

    @PostMapping("/addService")
    public void addService(@RequestBody MyService myService) {
        serviceService.addService(myService);
    }

    @DeleteMapping("/deleteService/{id}")
    public void deleteService(@PathVariable Long id) {
        serviceService.deleteService(id);
    }

}
