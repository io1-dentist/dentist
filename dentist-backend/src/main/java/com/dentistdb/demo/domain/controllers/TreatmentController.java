package com.dentistdb.demo.domain.controllers;

import com.dentistdb.demo.domain.data.Treatment;
import com.dentistdb.demo.domain.services.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TreatmentController {

    @Autowired
    private TreatmentService treatmentService;

    @GetMapping("/treatments")
    public List<Treatment> getAllTreatments() {
        return treatmentService.getAllTreatments();
    }

    @GetMapping("/treatmentById/{id}")
    public Treatment getTreatmentById(@PathVariable Long id) {
        return treatmentService.getTreatmentById(id);
    }

    @PostMapping("/addTreatment")
    public void addTreatment(@RequestBody Treatment treatment) {
        treatmentService.addTreatment(treatment);
    }

    @DeleteMapping("/deleteTreatment/{id}")
    public void deleteTreatment(@PathVariable Long id) {
        treatmentService.deleteTreatment(id);
    }
}
