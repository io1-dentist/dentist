package com.dentistdb.demo.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String secondName;
    private String birthDate;
    private String gender;
    private String postalCode;
    private String city;
    private String streetName;
    private String homeNumber;
    private long phoneNumber;
    private String PESEL;
    private String comments;
    @OneToMany(mappedBy="patient")
    @JsonIgnore
    private List<Visit> visitList;

    public Patient() {
    }

    public Patient(String firstName, String secondName, String birthDate, String gender, String postalCode, String city, String streetName, String homeNumber, long phoneNumber, String PESEL) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.postalCode = postalCode;
        this.city = city;
        this.streetName = streetName;
        this.homeNumber = homeNumber;
        this.phoneNumber = phoneNumber;
        this.PESEL = PESEL;
        this.comments = "";
    }

    public Patient(String firstName, String secondName, String birthDate, String gender, String postalCode, String city, String streetName, String homeNumber, long phoneNumber, String PESEL, String comments) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.gender = gender;
        this.postalCode = postalCode;
        this.city = city;
        this.streetName = streetName;
        this.homeNumber = homeNumber;
        this.phoneNumber = phoneNumber;
        this.PESEL = PESEL;
        this.comments = comments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<Visit> getVisitList() {
        return visitList;
    }

    public void setVisitList(List<Visit> visitList) {
        this.visitList = visitList;
    }

}
