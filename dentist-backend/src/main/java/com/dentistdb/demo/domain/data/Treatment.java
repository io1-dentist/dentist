package com.dentistdb.demo.domain.data;

import javax.persistence.*;

@Entity
public class Treatment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name="visit_id")
    private Visit visit;
    @ManyToOne
    @JoinColumn(name="myservice_id")
    private MyService myService;
    private String treatmentDescription;

    public Treatment() {
    }

    public Treatment(Visit visit, MyService service, String treatmentDescription) {
        this.visit = visit;
        this.myService = service;
        this.treatmentDescription = treatmentDescription;
    }

    public Treatment(String treatmentDescription) {
        this.treatmentDescription = treatmentDescription;
    }

    public long getId() {
        return id;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

    public MyService getMyService() {
        return myService;
    }

    public void setMyService(MyService service) {
        this.myService = service;
    }

    public String getTreatmentDescription() {
        return treatmentDescription;
    }

    public void setTreatmentDescription(String treatmentDescription) {
        this.treatmentDescription = treatmentDescription;
    }
}
