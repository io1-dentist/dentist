package com.dentistdb.demo.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String date;
    private String time;
    private String visitInfo;
    @ManyToOne
    @JoinColumn(name="patient_id")
    private Patient patient;
    @OneToMany(mappedBy = "visit") //@OneToMany(mappedBy = "visit", cascade = {CascadeType.ALL})
    @JsonIgnore
    private List<Treatment> myService;

    public Visit() {
    }

    public Visit(Patient patient, String date, String time) {
        this.patient = patient;
        this.date = date;
        this.time = time;
        this.visitInfo = "";
    }

    public Visit(Patient patient, String date, String time, String visitInfo) {
        this.patient = patient;
        this.date = date;
        this.time = time;
        this.visitInfo = visitInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVisitInfo() {
        return visitInfo;
    }

    public void setVisitInfo(String visitInfo) {
        this.visitInfo = visitInfo;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public List<Treatment> getMyService() {
        return myService;
    }

    public void setMyService(List<Treatment> myService) {
        this.myService = myService;
    }
}
