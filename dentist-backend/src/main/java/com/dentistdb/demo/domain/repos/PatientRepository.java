package com.dentistdb.demo.domain.repos;

import com.dentistdb.demo.domain.data.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<Patient,Long> {

    @Query("select e from Patient e where lower(e.secondName) like %:secondName%")
    List<Patient> findBySecondName(@Param("secondName") String secondName);
   // List<Patient> findBySecondName(String secondName);
    @Query("select e from Patient e where e.PESEL = :PESEL")
    Patient findByPesel(@Param("PESEL") String pesel);

}
