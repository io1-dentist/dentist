package com.dentistdb.demo.domain.repos;

import com.dentistdb.demo.domain.data.MyService;
import org.springframework.data.repository.CrudRepository;

public interface ServiceRepository extends CrudRepository<MyService,Long> {
    MyService findByServiceName(String serviceName);

}
