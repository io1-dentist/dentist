package com.dentistdb.demo.domain.repos;

import com.dentistdb.demo.domain.data.Treatment;
import org.springframework.data.repository.CrudRepository;

public interface TreatmentRepository extends CrudRepository<Treatment,Long> {

}
