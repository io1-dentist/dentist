package com.dentistdb.demo.domain.repos;

import com.dentistdb.demo.domain.data.Visit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitRepository extends CrudRepository<Visit,Long> {

}
