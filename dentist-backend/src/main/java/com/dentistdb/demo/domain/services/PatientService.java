package com.dentistdb.demo.domain.services;

import com.dentistdb.demo.domain.data.Patient;
import com.dentistdb.demo.domain.repos.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    public List<Patient> getAllPatients() {
        List<Patient> patients = new ArrayList<>();
        patientRepository.findAll().forEach(patients::add);
        return patients;
    }

    public Patient getPatientById(Long id) {
        return patientRepository.findById(id).orElse(null);
    }

    public Patient getPatientByPesel(String pesel) { return patientRepository.findByPesel(pesel); }

    public List<Patient> getPatientBySecondName(String secondName) {
        return patientRepository.findBySecondName(secondName.toLowerCase());
    }

    public void addPatient(Patient patient) {
        patientRepository.save(patient);
    }

    public void editPatient(Patient patient) {
        Patient patientDB = patientRepository.findById(patient.getId()).get();
        if(patientDB == null){
            return;
        }
        patientDB.setFirstName(patient.getFirstName());
        patientDB.setSecondName(patient.getSecondName());
        patientDB.setBirthDate(patient.getBirthDate());
        patientDB.setGender(patient.getGender());
        patientDB.setPostalCode(patient.getPostalCode());
        patientDB.setCity(patient.getCity());
        patientDB.setStreetName(patient.getStreetName());
        patientDB.setHomeNumber(patient.getHomeNumber());
        patientDB.setPhoneNumber(patient.getPhoneNumber());
        patientDB.setPESEL(patient.getPESEL());
        patientDB.setComments(patient.getComments());
        patientRepository.save(patientDB);
    }

    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

}
