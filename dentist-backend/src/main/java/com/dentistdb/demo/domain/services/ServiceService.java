package com.dentistdb.demo.domain.services;

import com.dentistdb.demo.domain.data.MyService;
import com.dentistdb.demo.domain.repos.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceService {

    @Autowired
    private ServiceRepository serviceRepository;

    public List<MyService> getAllServices() {
        List<MyService> services = new ArrayList<>();
        serviceRepository.findAll().forEach(services::add);
        return services;
    }

    public MyService getServiceById(Long id) {
        return serviceRepository.findById(id).orElse(null);
    }

    public MyService getServiceByName(String serviceName) {
        return serviceRepository.findByServiceName(serviceName);
    }

    public void addService(MyService myService) {
        serviceRepository.save(myService);
    }

    public void deleteService(Long id) {
        serviceRepository.deleteById(id);
    }
}
