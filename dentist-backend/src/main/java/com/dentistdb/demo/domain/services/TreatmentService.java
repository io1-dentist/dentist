package com.dentistdb.demo.domain.services;

import com.dentistdb.demo.domain.data.Treatment;
import com.dentistdb.demo.domain.repos.TreatmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TreatmentService {
    @Autowired
    private TreatmentRepository treatmentRepository;

    public List<Treatment> getAllTreatments() {
        List<Treatment> treatments = new ArrayList<>();
        treatmentRepository.findAll().forEach(treatments::add);
        return treatments;
    }

    public Treatment getTreatmentById(Long id) {
        return treatmentRepository.findById(id).orElse(null);
    }

    public void addTreatment(Treatment treatment) {
        treatmentRepository.save(treatment);
    }

    public void deleteTreatment(Long id) {
        treatmentRepository.deleteById(id);
    }

}
