package com.dentistdb.demo.domain.services;

import com.dentistdb.demo.domain.data.Visit;
import com.dentistdb.demo.domain.repos.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VisitService {

    @Autowired
    private VisitRepository visitRepository;

    public List<Visit> getAllVisits() {
        List<Visit> visits = new ArrayList<>();
        visitRepository.findAll().forEach(visits::add);
        return visits;
    }

    public Visit getVisitById(Long id) {
        return visitRepository.findById(id).orElse(null);
    }

    public void addVisit(Visit patient) {
        visitRepository.save(patient);
    }

    public void deleteVisit(Long id) {
        visitRepository.deleteById(id);
    }
}
