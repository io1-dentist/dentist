import { AppPage } from './app.po';
import { browser, by, element, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  
    //home
    it('should display h1 from /home', () => {
      page.navigateTo();
      expect(page.getHomeH1()).toEqual('DENTIST APP');
    });
  
    it('should display h3 from /home', () => {
      page.navigateTo();
      expect(page.getHomeH2()).toEqual('Witamy w naszej aplikacji dla dentystów.');
    });
  
    //pacjenci
    it('should display pacjenci button', () => {
      page.navigateTo();
      expect(page.getPacjenciButton().getText()).toEqual('Pacjenci');
    });
  
    it('should navigate to /pacjenci and display searchPatient h1', () => {
      page.navigateTo();
      page.getPacjenciButton().click();
  
      expect(page.getPacjenciSearchPatientH1()).toEqual('Szukaj pacjenta');
    });
  
    it('should navigate to /pacjenci and display addNewPatient h1', () => {
      page.navigateTo();
      page.getPacjenciButton().click();
  
      expect(page.getPacjenciAddNewPatientH1()).toEqual('Dodaj nowego pacjenta');
    });
  
    it('should navigate to /pacjenci and input search for Kowalski', () => {
      page.navigateTo();
      page.getPacjenciButton().click();
  
      let szukajInput = page.getSzukajPacjentaInput();
      szukajInput.sendKeys("Kowalski");
  
      expect(szukajInput.getAttribute('value')).toEqual('Kowalski');
    });
  
  
    it('should navigate to /pacjenci and search for Nowak', () => {
      page.navigateTo();
      page.getPacjenciButton().click();
  
      let szukajInput = page.getSzukajPacjentaInput();
      szukajInput.sendKeys("Nowak");
  
      page.getSzukajButton().click();
  
      let nazwisko = element.all(by.name('nazwisko'));
  
      expect(nazwisko.get(0).getText()).toBe('Nowak');
    });
  
    it('should navigate to /pacjenci and add Patient', () => {
      page.navigateTo();
      page.getPacjenciButton().click();
  
      let imieInput = element(by.css(".nameField"));
      imieInput.sendKeys("Adam");
  
      let nazwiskoInput = element(by.css(".secondNameField"));
      nazwiskoInput.sendKeys("Jakis");
  
      let dataInput = element(by.css(".birthDate"));
      dataInput.sendKeys("01/01/1999");
  
      let kodInput = element(by.css(".postalCode"));
      kodInput.sendKeys("40-300");
  
      let miastoInput = element(by.css(".cityField"));
      miastoInput.sendKeys("Katowice");
  
      let ulicaInput = element(by.css(".streetField"));
      ulicaInput.sendKeys("Ulica jakas");
  
      let numerDomuInput = element(by.css(".homeNumberField"));
      numerDomuInput.sendKeys("24");
  
      let telInput = element(by.css(".phoneField"));
      telInput.sendKeys("0000000");
  
      let peselInput = element(by.css(".peselField"));
      peselInput.sendKeys("123123123");
  
      page.getZapiszButton().click();
      element(by.buttonText('Zamknij')).click();
  
      let szukajInput = page.getSzukajPacjentaInput();
      szukajInput.sendKeys("Jakis");
  
      page.getSzukajButton().click();
  
      let pesel = element.all(by.name('pesel'));
      let imie = element.all(by.name('imie'));
      let nazwisko = element.all(by.name('nazwisko'));
      let miasto = element.all(by.name('miasto'));
      let ulica = element.all(by.name('ulica'));
      let numerDomu = element.all(by.name('numerDomu'));
  
      expect(pesel.get(0).getText()).toBe('123123123');
      expect(imie.get(0).getText()).toBe('Adam');
      expect(miasto.get(0).getText()).toBe('Katowice');
      expect(nazwisko.get(0).getText()).toBe('Jakis');
      expect(ulica.get(0).getText()).toBe('Ulica jakas');
      expect(numerDomu.get(0).getText()).toBe('24');
    });
  
  //wizyty
  it('should navigate to /wizyty and add Wizyta', () => {
    page.navigateTo();
    page.getWizytyButton().click();

    var listBefore = element.all(by.css('.mojeWizyty'));

    listBefore.count().then(function (amount) {
      let peselInput = element(by.css(".inputField"));
      peselInput.sendKeys("12345678900");

      let dataInput = element(by.css(".dataField"));
      dataInput.sendKeys("01.01.2020");

      let godzinaInput = element(by.css(".godzinaField"));
      godzinaInput.sendKeys("12:00");

      page.getStworzWizyteButton().click();
      page.geOdswiezWizytyButton().click();

      var list = element.all(by.css('.mojeWizyty'));
      expect(list.count()).toBe(amount + 1);
    });

  });

  //uslugi
  it('should navigate to /uslugi and add Usluga', () => {
    page.navigateTo();
    page.getUslugiButton().click();

    var listBefore = element.all(by.css('.mojeUslugi'));

    listBefore.count().then(function (amount) {
      let nameInput = element(by.css(".serviceName"));
      nameInput.sendKeys("usluga 1");

      let priceInput = element(by.css(".servicePrice"));
      priceInput.sendKeys("20.20");

      page.getZapiszButton().click();
      page.getOdswiezUslugiButton().click();

      var list = element.all(by.css('.mojeUslugi'));
      expect(list.count()).toBe(amount + 1);
    });
  });

  //pacjent-profil
  it('should pick patient and edit Kowalski data', () => {
    page.navigateTo();
    page.getPacjenciButton().click();

    let szukajInput = page.getSzukajPacjentaInput();
    szukajInput.sendKeys("Kowalski");

    page.getSzukajButton().click();
    page.getWybierzButton().click();

    let imieInput = element(by.css(".nameField"));
    imieInput.sendKeys("Adam");

    let nazwiskoInput = element(by.css(".secondNameField"));
    nazwiskoInput.sendKeys("dodane");

    let dataInput = element(by.css(".birthDate"));
    dataInput.sendKeys("01/01/1999");

    let kodInput = element(by.css(".postalCode"));
    kodInput.sendKeys("40-300");

    let miastoInput = element(by.css(".cityField"));
    miastoInput.sendKeys("Katowice");

    let ulicaInput = element(by.css(".streetField"));
    ulicaInput.sendKeys("Ulica jakas");

    let numerDomuInput = element(by.css(".homeNumberField"));
    numerDomuInput.sendKeys("24");

    let telInput = element(by.css(".phoneField"));
    telInput.sendKeys("0000000");

    let peselInput = element(by.css(".peselField"));
    peselInput.sendKeys("123123123");

    page.getNadpiszButton().click();

    szukajInput.sendKeys("dodane");
    page.getSzukajButton().click();

    let nazwisko = element.all(by.name('nazwisko'));
    expect(nazwisko.get(0).getText()).toContain('dodane');
  });

});
