import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  //home
  getHomeH1(): Promise<string> {
    return element(by.css('.mainPage .mainPageText h1')).getText() as Promise<string>;
  }

  getHomeH2(): Promise<string> {
    return element(by.css('.mainPage .mainPageText h3')).getText() as Promise<string>;
  }

  //pacjenci
  getPacjenciButton() {
    return element(by.buttonText("Pacjenci"));
  }

  getPacjenciSearchPatientH1(): Promise<string> {
    return element(by.css('.mainPatients .searchPatient h1')).getText() as Promise<string>;
  }

  getPacjenciAddNewPatientH1(): Promise<string> {
    return element(by.css('.mainPatients .addNewPatient h1')).getText() as Promise<string>;
  }

  getSzukajPacjentaInput() {
    return element(by.className("patientSearchBar"));
  }

  getSzukajButton() {
    return element(by.buttonText('Szukaj'));
  }

  getZapiszButton() {
    return element(by.buttonText('Zapisz'));
  }

  getOdswiezButton() {
    return element(by.buttonText('Odśwież'));
  }

  getWybierzButton() {
    return element(by.buttonText('Wybierz'));
  }

  //wizyty
  getWizytyButton() {
    return element(by.buttonText("Wizyty"));
  }

  geOdswiezWizytyButton() {
    return element(by.buttonText("Odśwież"));
  }

  getStworzWizyteButton() {
    return element(by.buttonText('Stwórz wizytę'));
  }

  //uslugi
  getUslugiButton() {
    return element(by.buttonText("Usługi"));
  }

  getOdswiezUslugiButton() {
    return element(by.buttonText("Odśwież"));
  }

  //profilPacjenta
  getNadpiszButton() {
    return element(by.buttonText("Nadpisz dane pacjenta"));
  }

}
