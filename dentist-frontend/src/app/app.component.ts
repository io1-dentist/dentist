import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Nwta'; // ?????

  constructor(private router: Router) { }

  home() {
    this.router.navigate(['/home']);
  }

  pierwsza() {
    this.router.navigate(['/pierwsza']);
  }

  wizyty() {
    this.router.navigate(['/wizyty']);
  }

  pacjenci() {
    this.router.navigate(['/pacjenci']);
  }

  uslugi() {
    this.router.navigate(['/uslugi']);
  }

  zabiegi() {
    this.router.navigate(['/zabiegi']);
  }
}
