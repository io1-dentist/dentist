import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';

const materialModules = [
  BrowserAnimationsModule,
  MatButtonModule,
  MatMenuModule,
  MatSortModule,
  MatTableModule,
  MatInputModule,
  MatPaginatorModule,
  MatRadioModule,
];

@NgModule({
  declarations: [],
  imports: materialModules,
  exports: materialModules
})



export class MaterialModule { }
