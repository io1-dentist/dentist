import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Patient } from './patient-model/patient-model';
import { PatientServiceService } from './patient-service/patient-service.service';
import { Router, NavigationEnd } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { ModalService } from '../_modal';
import { PacjentProfilComponent } from '../pacjent-profil/pacjent-profil.component';

@Component({
  selector: 'app-pacjenci',
  templateUrl: './pacjenci.component.html',
  styleUrls: ['./pacjenci.component.scss'],
})

export class PacjenciComponent implements OnInit {
  patients: Array<Patient>;
  displayedColumns: string[] = ['id', 'firstName', 'secondName', 'city', 'streetName', 'homeNumber', 'link'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private patientService: PatientServiceService, private router: Router, private modalService: ModalService) {
    this.router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          this.getAllPatients();
        }
      }
    );
  }

  ngOnInit(): void {
  }

  getAllPatients() {
    this.patientService.getAllPatients()
      .subscribe(
        data => {
          console.log(data);
          this.patients = data;
        },
        error => console.log(error));
  }

  @ViewChild('szukajInput') szukajInput: ElementRef;
  getPatientsBySecondName(event: any) {
    let inputValue = this.szukajInput.nativeElement.value;

    console.log(inputValue);
    this.patientService.getPatientBySecondName(inputValue)
      .subscribe(
        data => {
          console.log(data);
          this.patients = data;
        },
        error => console.log(error));
  }

  @ViewChild('firstNameInput') firstNameInput: ElementRef;
  @ViewChild('secondNameInput') secondNameInput: ElementRef;
  @ViewChild('birthDateInput') birthDateInput: ElementRef;
  @ViewChild('postalCodeInput') postalCodeInput: ElementRef;
  @ViewChild('cityInput') cityInput: ElementRef;
  @ViewChild('streetInput') streetInput: ElementRef;
  @ViewChild('homeNumberInput') homeNumberInput: ElementRef;
  @ViewChild('phoneNumberInput') phoneNumberInput: ElementRef;
  @ViewChild('peselInput') peselInput: ElementRef;
  @ViewChild('commentsInput') commentsInput: ElementRef;

  genders: string[] = ['Mężczyzna', 'Kobieta'];
  pickedGender: string;
  addedPatient: string;

  addPatient(patient: Patient) {
    let firstName = this.firstNameInput.nativeElement.value;
    let secondName = this.secondNameInput.nativeElement.value;
    let birthDate = this.birthDateInput.nativeElement.value;
    let postalCode = this.postalCodeInput.nativeElement.value;
    let city = this.cityInput.nativeElement.value;
    let street = this.streetInput.nativeElement.value;
    let homeNumber = this.homeNumberInput.nativeElement.value;
    let phoneNumber = this.phoneNumberInput.nativeElement.value;
    let pesel = this.peselInput.nativeElement.value;
    let comments = this.commentsInput.nativeElement.value;

    patient.firstName = firstName;
    patient.secondName = secondName;
    patient.birthDate = birthDate;
    patient.gender = this.pickedGender;
    patient.postalCode = postalCode;
    patient.city = city;
    patient.streetName = street;
    patient.homeNumber = homeNumber;
    patient.phoneNumber = phoneNumber;
    patient.pesel = pesel;
    patient.comments = comments;

    this.patientService.addPatient(patient)
      .subscribe(
        data => {
          console.log(data);
        },
        error => console.log(error));

    this.firstNameInput.nativeElement.value = "";
    this.secondNameInput.nativeElement.value = "";
    this.birthDateInput.nativeElement.value = "";
    this.postalCodeInput.nativeElement.value = "";
    this.cityInput.nativeElement.value = "";
    this.streetInput.nativeElement.value = "";
    this.homeNumberInput.nativeElement.value = "";
    this.phoneNumberInput.nativeElement.value = "";
    this.peselInput.nativeElement.value = "";
    this.commentsInput.nativeElement.value = "";

    this.addedPatient = patient.firstName + " " + patient.secondName;
    this.openModal('dodanoPacjentaModal');
  }

  redirect(id: string) {
    sessionStorage.setItem('selectedId', id);
    this.router.navigate(['/pacjentProfil']);
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
