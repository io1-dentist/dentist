export class Patient {
    id: number;
    firstName: string;
    secondName: string;
    birthDate: string;
    gender: string;
    postalCode: string;
    city: string;
    streetName: string;
    homeNumber: string;
    phoneNumber: number;
    comments: string;
    pesel: string;
}
