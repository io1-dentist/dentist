import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Patient } from "../patient-model/patient-model";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class PatientServiceService {
  private url = environment.BACKEND_ADDRESS;

  constructor(private http: HttpClient) {}

  public getAllPatients(): Observable<any> {
    return this.http.get(`${this.url}/patients`);
  }

  public getPatientById(id): Observable<any> {
    return this.http.get(`${this.url}/patientById/` + id);
  }

  public getPatientBySecondName(secondName: string): Observable<any> {
    return this.http.get(`${this.url}/patientBySecondName/` + secondName);
  }

  public getPatientByPesel(pesel: string): Observable<any> {
    return this.http.get(`${this.url}/patientByPesel/` + pesel);
  }

  public addPatient(patient: any): Observable<any> {
    return this.http.post(`${this.url}/addPatient`, patient);
  }

  public editPatient(patient: any): Observable<any> {
    return this.http.put(`${this.url}/editPatient`, patient);
  }

  public deletePatient(id): Observable<any> {
    return this.http.delete(`${this.url}/deletePatient/` + id);
  }
}
