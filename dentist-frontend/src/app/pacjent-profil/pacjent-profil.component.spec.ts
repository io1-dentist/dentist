import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacjentProfilComponent } from './pacjent-profil.component';

describe('PacjentProfilComponent', () => {
  let component: PacjentProfilComponent;
  let fixture: ComponentFixture<PacjentProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacjentProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacjentProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
