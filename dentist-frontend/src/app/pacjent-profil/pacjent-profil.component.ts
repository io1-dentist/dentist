import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Patient } from '../pacjenci/patient-model/patient-model';
import { PatientServiceService } from '../pacjenci/patient-service/patient-service.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-pacjent-profil',
  templateUrl: './pacjent-profil.component.html',
  styleUrls: ['./pacjent-profil.component.scss']
})
export class PacjentProfilComponent implements OnInit {

  selectedId;
  patient: Patient = new Patient();
  genders: string[] = ['Mężczyzna', 'Kobieta'];

  constructor(private patientService: PatientServiceService, private router: Router) {
    this.selectedId = sessionStorage.getItem('selectedId');
    console.log('id: ' + this.selectedId);
  }

  ngOnInit(): void {
    this.patientService.getPatientById(this.selectedId)
      .subscribe(
        data => {
          console.log(data);
          this.patient = data;
        },
        error => console.log(error));
  }

  @ViewChild('firstNameInput') firstNameInput: ElementRef;
  @ViewChild('secondNameInput') secondNameInput: ElementRef;
  @ViewChild('birthDateInput') birthDateInput: ElementRef;
  @ViewChild('postalCodeInput') postalCodeInput: ElementRef;
  @ViewChild('cityInput') cityInput: ElementRef;
  @ViewChild('streetInput') streetInput: ElementRef;
  @ViewChild('homeNumberInput') homeNumberInput: ElementRef;
  @ViewChild('phoneNumberInput') phoneNumberInput: ElementRef;
  @ViewChild('peselInput') peselInput: ElementRef;
  @ViewChild('commentsInput') commentsInput: ElementRef;


  editPatient(patient: Patient) {
    console.log(patient);
    let id = this.patient.id;
    let firstName = this.firstNameInput.nativeElement.value;
    let secondName = this.secondNameInput.nativeElement.value;
    let birthDate = this.birthDateInput.nativeElement.value;
    let gender = this.patient.gender;
    let postalCode = this.postalCodeInput.nativeElement.value;
    let city = this.cityInput.nativeElement.value;
    let street = this.streetInput.nativeElement.value;
    let homeNumber = this.homeNumberInput.nativeElement.value;
    let phoneNumber = this.phoneNumberInput.nativeElement.value;
    let pesel = this.peselInput.nativeElement.value;
    let comments = this.commentsInput.nativeElement.value;

    patient.id = id;
    patient.firstName = firstName;
    patient.secondName = secondName;
    patient.birthDate = birthDate;
    patient.gender = gender;
    patient.postalCode = postalCode;
    patient.city = city;
    patient.streetName = street;
    patient.homeNumber = homeNumber;
    patient.phoneNumber = phoneNumber;
    patient.pesel = pesel;
    patient.comments = comments;

    this.patientService.editPatient(patient).subscribe(data => { console.log(data); }, error => console.log(error));

    // ewentualny modal

    this.redirectToPatients();
  }

  deletePatient(patent: Patient) {
    this.patientService.deletePatient(this.patient.id).subscribe(data => { console.log(data); }, error => console.log(error));
    this.redirectToPatients();
  }

  redirectToPatients() {
    this.router.navigate(['/pacjenci']);
  }

  redirect(peselNumber: string) {
    sessionStorage.setItem('SelectedPesel', peselNumber);
    this.router.navigate(['/wizyty']);
  }

}
