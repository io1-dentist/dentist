import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PierwszaComponent } from './pierwsza/pierwsza.component';
import { HomeComponent } from './home/home.component';
import { WizytyComponent } from './wizyty/wizyty.component';
import { PacjenciComponent } from './pacjenci/pacjenci.component';
import { UslugiComponent } from './uslugi/uslugi.component';
import { ZabiegiComponent } from './zabiegi/zabiegi.component';
import { PacjentProfilComponent } from './pacjent-profil/pacjent-profil.component';

const routes: Routes = [
  { path: 'pierwsza', component: PierwszaComponent },
  { path: 'home', component: HomeComponent },
  { path: 'wizyty', component: WizytyComponent },
  { path: 'pacjenci', component: PacjenciComponent },
  { path: 'uslugi', component: UslugiComponent },
  { path: 'zabiegi', component: ZabiegiComponent },
  { path: 'pacjentProfil', component: PacjentProfilComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class RoutingModule { }

export const routingComponents = [HomeComponent, PierwszaComponent, WizytyComponent, PacjenciComponent, UslugiComponent, ZabiegiComponent, PacjentProfilComponent]
