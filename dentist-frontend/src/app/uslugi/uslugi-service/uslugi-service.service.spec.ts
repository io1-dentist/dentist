import { TestBed } from '@angular/core/testing';

import { UslugiServiceService } from './uslugi-service.service';

describe('PatientServiceService', () => {
  let service: UslugiServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UslugiServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
