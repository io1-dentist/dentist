import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class UslugiServiceService {
  private url = environment.BACKEND_ADDRESS;

  constructor(private http: HttpClient) {}

  public getAllUslugi(): Observable<any> {
    return this.http.get(`${this.url}/services`);
  }

  public addUsluga(usluga: any): Observable<any> {
    return this.http.post(`${this.url}/addService`, usluga);
  }

  public deleteUsluga(id): Observable<any> {
    return this.http.delete(`${this.url}/deleteService/` + id);
  }
}
