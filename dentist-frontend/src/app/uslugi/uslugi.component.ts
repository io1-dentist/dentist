import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Usluga } from './uslugi-model/uslugi-model';
import { UslugiServiceService } from './uslugi-service/uslugi-service.service';

@Component({
  selector: 'app-uslugi',
  templateUrl: './uslugi.component.html',
  styleUrls: ['./uslugi.component.scss']
})
export class UslugiComponent implements OnInit {

  constructor(private uslugiService: UslugiServiceService) { }

  ngOnInit(): void {
    this.uslugiService.getAllUslugi()
      .subscribe(data => {
        this.mojeUslugi = data;
      });
  }

  @ViewChild('serviceNameInput') serviceNameInput: ElementRef;
  @ViewChild('servicePriceInput') servicePriceInput: ElementRef;
  uslugi: string[] = ['Uzębienie', 'Proteza'];
  pickedUsluga: string;
  mojeUslugi;

  onZapisz(usluga: Usluga) {
    let name = this.serviceNameInput.nativeElement.value;
    let price = this.servicePriceInput.nativeElement.value;

    usluga.serviceName = name;
    usluga.price = price;
    usluga.serviceType = this.pickedUsluga;

    console.log(usluga.serviceType);

    this.uslugiService.addUsluga(usluga)
      .subscribe(
        data => {
          console.log(data);
          this.odswiezUslugi();
        },
        error => console.log(error));

    this.serviceNameInput.nativeElement.value = "";
    this.servicePriceInput.nativeElement.value = "";
  }

  odswiezUslugi() {
    this.uslugiService.getAllUslugi()
      .subscribe(data => {
        this.mojeUslugi = data;
      });
  }

  deleteService(id: number) {
    this.uslugiService.deleteUsluga(id).subscribe(data => { console.log(data); this.odswiezUslugi(); }, error => console.log(error));
  }

}
