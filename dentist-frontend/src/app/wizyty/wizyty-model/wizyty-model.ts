import {Patient} from '../../pacjenci/patient-model/patient-model';

export class Wizyta{
  id: number;
  date: string;
  time: string;
  patient: Patient;
}
