import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class WizytyServiceService {
  private url = environment.BACKEND_ADDRESS;

  constructor(private http: HttpClient) {}

  public getAllWizyty(): Observable<any> {
    return this.http.get(`${this.url}/visits`);
  }

  public addWizyta(wizyta: any): Observable<any> {
    return this.http.post(`${this.url}/addVisit`, wizyta);
  }

  public deleteWizyta(id): Observable<any> {
    return this.http.delete(`${this.url}/deleteVisit/` + id);
  }

}
