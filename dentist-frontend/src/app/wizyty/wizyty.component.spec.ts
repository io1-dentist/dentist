import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizytyComponent } from './wizyty.component';

describe('WizytyComponent', () => {
  let component: WizytyComponent;
  let fixture: ComponentFixture<WizytyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WizytyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WizytyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
