import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { WizytyServiceService } from './wizyty-service/wizyty-service';
import { Wizyta } from './wizyty-model/wizyty-model';
import { PatientServiceService } from '../pacjenci/patient-service/patient-service.service';
import { ModalService } from '../_modal';

@Component({
  selector: 'app-wizyty',
  templateUrl: './wizyty.component.html',
  styleUrls: ['./wizyty.component.scss']
})
export class WizytyComponent implements OnInit {

  @ViewChild('dateInput') dateInput: ElementRef;
  @ViewChild('timeInput') timeInput: ElementRef;
  @ViewChild('peselInput') peselInput: ElementRef;

  selectedPesel;
  mojeWizyty;

  constructor(private wizytyService: WizytyServiceService, private patientService: PatientServiceService, private modalService: ModalService) {
    this.selectedPesel = sessionStorage.getItem('SelectedPesel');
  }

  ngOnInit(): void {
    this.wizytyService.getAllWizyty().subscribe(data => { this.mojeWizyty = data; });
  }

  addVisit(wizyta: Wizyta) {
    let date = this.dateInput.nativeElement.value;
    let time = this.timeInput.nativeElement.value;
    let pesel = this.peselInput.nativeElement.value;

    if (date !== null && date !== '' && time !== null && time !== '' && pesel !== null && pesel !== '') {

      const getPatient = this.patientService.getPatientByPesel(pesel).toPromise();
      getPatient.then(data => {
        wizyta.date = date;
        wizyta.time = time;
        wizyta.patient = data;

        this.wizytyService.addWizyta(wizyta)
          .subscribe(data => {
            console.log(data);
            this.refreshVisits();
          }, error => console.log(error));

      });
    } else {
      this.openModal('pustePoleModal');
    }
  }

  refreshVisits() {
    this.wizytyService.getAllWizyty().subscribe(data => { this.mojeWizyty = data; });
  }

  removeVisit(id: number) {
    this.wizytyService.deleteWizyta(id).subscribe(data => { console.log(data); this.refreshVisits(); }, error => console.log(error));
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
